FROM registry.access.redhat.com/ubi8:latest

RUN set -ex && \
    adduser -d /xmrig mining && \
    rpm -ivh https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm && \
    yum -y install ruby gcc python3-devel python3 python3-pip java-11-openjdk maven nodejs && \
    python3 -m ensurepip && \
    gem install ftpd -v 0.2.1 && \
    yum remove -y ruby gcc && \
    yum autoremove -y && \
    yum clean all && \
    rm -rf /var/cache/yum 

USER mining
ENTRYPOINT /bin/false
    

